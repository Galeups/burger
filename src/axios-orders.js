import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://galeups-react-my-burger.firebaseio.com/',
});

export default instance;