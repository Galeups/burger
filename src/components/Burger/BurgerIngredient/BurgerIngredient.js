import React, { Component } from 'react';
import propTypes from 'prop-types';

import clasess from './BurgerIngredient.css';


class BurgerIngredient extends Component {

  render() {
    let ingredient = null;

    switch (this.props.type) {

      case ('bread-bottom'):
        ingredient = <div className={clasess.BreadBottom} />;
        break;

      case ('bread-top'):
        ingredient = (
          <div className={clasess.BreadTop}>
            <div className={clasess.Seeds1} />
            <div className={clasess.Seeds2} />
          </div>
        );
        break;

      case ('meat'):
        ingredient = <div className={clasess.Meat} />;
        break;

      case ('cheese'):
        ingredient = <div className={clasess.Cheese} />;
        break;

      case ('bacon'):
        ingredient = <div className={clasess.Bacon} />;
        break;

      case ('salad'):
        ingredient = <div className={clasess.Salad} />;
        break;

      default:
        ingredient = null;
    }

    return ingredient;
  }

};

BurgerIngredient.propTypes = {
  type: propTypes.string.isRequired,
};

export default BurgerIngredient;