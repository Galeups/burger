import React, { Component } from 'react';
import AuxHoc from '../../../hoc/AuxHoc/AuxHoc';
import Button from '../../UI/Button/Button';

class OrderSummary extends Component {

  render() {
    const ingredientSummary = Object.keys(this.props.ingredients)
      .map(igKey => {
        return (
          <li key={igKey}>
            <span style={{ textTransform: 'capitalize' }}>{igKey}</span>: {this.props.ingredients[igKey]}
          </li>
        );
      });

    return (
      <AuxHoc>
        <h3>Your Order</h3>
        <p>A delicious burger with the folllowing ingredients:</p>
        <ul>
          {ingredientSummary}
        </ul>
        <p>Continue to Checkout?</p>
        <p><strong>Total price: {this.props.price.toFixed(2)}</strong></p>
        <Button btnType="Danger" clicked={this.props.purchaseCancelled}>CANCEL</Button>
        <Button btnType="Success" clicked={this.props.purchaseContinued}>CONTINUE</Button>

      </AuxHoc>
    );
  }
}

export default OrderSummary;