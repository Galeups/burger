export const updateObject = (oldObject, updateProprties) => {
  return {
    ...oldObject,
    ...updateProprties,
  };
};

export const checkValidity = (value, rules) => {
  let isValid = true;

  if (rules !== undefined) {
    return true;
  }

  if (rules.required) {
    isValid = value.trim() !== '' && isValid;
  }

  if (rules.minLength) {
    isValid = value.length >= rules.minLength && isValid;
  }

  if (rules.maxLength) {
    isValid = value.length >= rules.maxLength && isValid;
  }

  return isValid;
}